﻿
module Program =
    open System
    open Akka.Actor
    open Akka.FSharp

    open FetchingApp
   
    (* Flush to the disk all resources which were previously cached by the CoordActor *)
    let private doFlushCachedResources (coordActorRef: IActorRef) (path: String): Async<unit> =
        async {
            let! flushResult = 
                coordActorRef <? CoordActor.Request.FlushCache path
                    |> Async.Catch  

            return 
               match flushResult with
                | Choice1Of2 response -> 
                    match response with
                    | CoordActor.Response.Flushed pairs ->
                        printfn "Cache has been flushed to disk %A" response

                        // displaying results for individual resource
                        pairs |> Seq.iter (fun(pair) ->
                            let (resourceId, choice) = pair

                            match choice with
                            | Choice1Of2 _ -> printfn "Resource %A has been flush successfully" resourceId
                            | Choice2Of2 exn -> printfn "Resource %A flush has been failed %A" resourceId exn
                        )
                    | e -> printfn "Unexpected response received from coordination actor %A" e
                | Choice2Of2 exn -> 
                    printfn "Cache flush failed %A %A" flushResult exn
        }

    (* Requestes given resource by its id *)
    let private doRequestResource (coordActorRef: IActorRef) (resourceId: ResourceId) = 
        let result = 
            coordActorRef <? CoordActor.Request.Get(resourceId) 
                |> Async.Catch 
                |> Async.RunSynchronously

        match result with
        | Choice1Of2 response -> printfn "Response received %A" response
        | Choice2Of2 exn -> printfn "Exception %A" exn

    (* Spawn and then registers N worker actors (based on app.ini) within CoordActor *)
    let private doRegisterWorkerActors (coordActorRef: IActorRef) (system: ActorSystem) =
        let fetchingActors = seq { 
            for i in 1 .. int Settings.worker.amount -> 
                (FetchingActor.create system ("fetching-actor-" + string i)) 
        }

        // registering actors within coordination actor
        let registeringFuture = 
            Seq.map (fun ref -> coordActorRef <? CoordActor.Request.Register ref) fetchingActors 
                |> Async.Parallel 
                |> Async.Catch 
                |> Async.RunSynchronously

        match registeringFuture with
        | Choice1Of2 response -> printfn "Worker registration finished with response %A" response
        | Choice2Of2 exn -> printfn "Worker registeration failed %A" exn

    (* Request CoordActor to stop and uregister all currently known workers *)
    let private doStopWorkers (coordActorRef: IActorRef) = 
        coordActorRef <? CoordActor.Request.StopWorkers 
            |> Async.Catch

    (* Remove the single resource from the CoordActor cache *)
    let private doRemoveResource (coordActorRef: IActorRef)(resource: ResourceId): Async<CoordActor.Response> =
        async {
            let _ = printfn "Removing resource %A %A" resource coordActorRef
            let! result = coordActorRef <? CoordActor.Request.Delete resource
            let _ = printfn "Response received"
            return result
        } 

    (* Remove all resources from the CoordActor cache *)
    let private doRemoveResources (coordActorRef: IActorRef): Async<CoordActor.Response[]> = 
        async {
            let! response = coordActorRef <? CoordActor.Request.ListResources
            let _ = printfn "Response received %A" response
            return! 
                match response with
                    | CoordActor.Response.ResourcesList(resourcesList) ->    
                        printfn "Removal list %A" resourcesList
                        List.map (doRemoveResource coordActorRef) resourcesList |> Async.Parallel 
                    | e ->
                        raise <| new ApplicationException(FailureType.Unknown None)
        }

    [<EntryPoint>]
    let main argv = 
        use system = System.create "fetching-app-system" (Configuration.load())
        printfn "System started %A" system
       
        let coordActorRef = CoordActor.create system "coordination-actor"

        doRegisterWorkerActors coordActorRef system

        let firstResourceId: ResourceId = {id= "test"; url="http://google.com"}

        // first time request should incude resource fetch on the worker side
        printfn "First attempt"

        let firstReqSelfTime = probeTime( fun() -> doRequestResource coordActorRef firstResourceId )
        printfn "Executed within %A" firstReqSelfTime.TotalSeconds

        // this time, resource should be retrieved from cache
        printfn "The second one..."
        let secondReqSelfTime = probeTime( fun() -> doRequestResource coordActorRef firstResourceId )
        printfn "Executed within %A" secondReqSelfTime.TotalSeconds

        doFlushCachedResources coordActorRef Settings.worker.cacheDirectoryPath |> Async.RunSynchronously

        match doStopWorkers coordActorRef |> Async.Catch |> Async.RunSynchronously with
        | Choice1Of2 result -> printfn "All workers have been stopped" 
        | Choice2Of2 exn -> printfn "There was some problem during attent to shutdown working threads"

        let removedResources = doRemoveResources coordActorRef |> Async.RunSynchronously |> List.ofArray
        let folder (total: int) (response: CoordActor.Response) : int =
            match response with
            | CoordActor.Response.Done -> 1 + total
            | _ -> total

        let removedSuccessfully = removedResources |> (List.fold folder 0)
        let removedTotal = List.length removedResources
        printfn "Total amount of successfuly removed resources: %A / %A"  removedSuccessfully removedTotal

        printfn "The third one..."
        let secondReqSelfTime = probeTime( fun() -> doRequestResource coordActorRef firstResourceId )
        printfn "Executed within %A" secondReqSelfTime.TotalSeconds

        0