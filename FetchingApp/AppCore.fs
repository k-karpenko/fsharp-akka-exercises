﻿namespace FetchingApp

open System
open FSharp.Configuration

[<AutoOpen>]
module DomainTypes =
    type ResourceId = {url:string; id:string }
    type Resource = {resource: ResourceId; resourceData: byte[] }
   
    type FailureType = 
        | NoResourcesAvailable
        | RecordNotFound
        | NotAllowed
        | Unknown of string option 

    type ApplicationException(failureType: FailureType) =
        inherit Exception()

    type Settings = IniFile<"app.ini">

    let probeTime<'a, 'b>(fn: unit -> unit): TimeSpan = 
        let started = System.DateTime.UtcNow.ToUniversalTime()
        fn()
        System.DateTime.UtcNow.ToUniversalTime() - started

module Async = 
    let map f m =
        async {
            let! v = m
            return f v
        }

    let flatMap f m =
        async {
            let! v = m
            return! f v
        }