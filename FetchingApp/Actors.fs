﻿namespace FetchingApp

open System
open System.IO
open Akka.FSharp
open Akka.Actor

module FetchingActor =

    type Request = 
        | Fetch of ResourceId 
        | Remove of ResourceId 

    type Response = 
        | Fetched of Resource
        | Error of string
    
    let receive (mailbox: Actor<'a>) msg =
        let sender = mailbox.Sender()
        match msg with
        | Fetch resourceId -> 
            printfn "Fetch command received %A" resourceId
            async {
                let fetchedData = (Array.create 500 (byte 1)) 
                let response = { resource = resourceId; resourceData = fetchedData}
                let! sleep = Async.Sleep(int Settings.worker.delay)
                sender <! response
            } |> Async.RunSynchronously

        | Remove resourceId -> printfn "Remove command received %A" resourceId 

    let create (system: ActorSystem) (name: string) =
        spawn system name (actorOf2 receive)

module CoordActor =

    type Request =
        | ListResources
        | FlushCache of string
        | Get of ResourceId
        | Register of IActorRef
        | StopWorkers
        | Delete of ResourceId
        | Put of ResourceId * Resource
    
    type Response =
        | ResourcesList of ResourceId list
        | Resolved of Resource
        | Registered 
        | Stopped
        | Done
        | Flushed of (ResourceId * Choice<unit, exn>) seq
        | Error of FailureType

    let private doFlushResource (path: string) (pair: ResourceId * Resource): Async<ResourceId * Choice<unit, exn>> =
        let (id, resource) = pair
        printfn "Do flush resource %A %A" path id

        let targetResourcePath = Path.Combine(path, id.id + ".dmp")
        printfn "Target resource path %A" targetResourcePath
        let targetResource = System.IO.File.Open( targetResourcePath, FileMode.OpenOrCreate)
        printfn "Target resource %A" targetResource

        let write (targetResource: FileStream): Async<unit> = 
            targetResource.AsyncWrite(resource.resourceData, 0, resource.resourceData.Length) 
        let flush (targetResource: FileStream): Async<unit> = 
            Async.AwaitTask(targetResource.FlushAsync())
        async {
            let! writeAndFlush = 
                (Async.flatMap 
                    (fun _ -> 
                        printfn "Flushing changes to resources %A" targetResourcePath
                        flush targetResource
                    ) 
                    (write targetResource) 
                ) |> Async.Catch

            let _ = targetResource.Close()

            return (id, writeAndFlush)
        }

    let private selectWorker (workers: IActorRef list) =
        let rnd = System.Random()

        if ( workers.IsEmpty ) then 
            None
        else 
            Some(workers.Item(rnd.Next(0, (List.length workers) - 1)))

    let receive (mailbox: Actor<Request>) =
        let rec loop (workers: IActorRef list) (cache: Map<ResourceId, Resource>)= actor {
            let! msg = mailbox.Receive() 
            let sender = mailbox.Sender()

            match msg with
            | FlushCache path ->
                async {
                   let! result = cache |> Map.toList |> List.map (doFlushResource path) |> Async.Parallel
                   return Response.Flushed(result)
                } |!> sender

                return! (loop workers cache)
            | Put (resourceId, resource) ->
                printfn "`Put` request received for resource %A" resourceId
                sender <! Response.Done
                return! (loop workers (cache.Add(resourceId, resource)))
            
            | Get resourceId -> 
                match (cache.TryFind(resourceId)) with
                | Some resource ->
                    printfn "Resource resolved from cache %A" resource
                    sender <! Response.Resolved(resource)
                | None ->
                    printfn "`Get` command received %A" resourceId
                    match selectWorker workers with
                    | None -> 
                        sender <! Response.Error(FailureType.NoResourcesAvailable)
                    | Some selectedWorker ->
                        async {
                            let! result = (selectedWorker <? FetchingActor.Request.Fetch resourceId) 
                            printfn "Fetch result received at coord actor %A" result
                            let! _ = mailbox.Self <? Put(resourceId, result)
                            return Response.Resolved(result)
                        } |!> sender

                return! loop workers cache

            | Delete resourceId ->
                printfn "`Delete` command received %A" resourceId

                let (response, updatedCache) = 
                    match cache.TryFind(resourceId) with
                        | None -> (Response.Error FailureType.RecordNotFound, cache)
                        | Some resource -> (Response.Done, cache.Remove(resourceId))
                
                sender <! response

                return! loop workers updatedCache

            | StopWorkers ->
                workers 
                    |> List.map (fun w -> w <! PoisonPill.Instance) 
                    |> ignore
                
                sender <! Response.Stopped

                return! loop List.empty cache

            | Register actorRef ->
                printfn "ActorRef register request received"
                sender <! Response.Registered
                return! loop (actorRef :: workers) cache
            | ListResources ->
                sender <! Response.ResourcesList (List.map (fun (key, value) -> key) (Map.toList cache))
                return! loop workers cache
        }

        loop(List.empty)(Map.empty)

    let create (system: ActorSystem) (name: string) = 
        spawn system name receive
